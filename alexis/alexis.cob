      *****************************************************************
      * Program name:    projet                               
      * Original author: Alexis Vatin                                          
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. gesticksAlexis.
       AUTHOR. Alexis Vatin.
      *****************************************************************
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION.
       FILE-CONTROL. 
           
           select fentr assign to "entreprise.dat"
           organization SEQUENTIAL
           access mode is sequential
           file status is cr_fentr.

           select fempl assign to "employes.dat"
           organization indexed
           access mode is dynamic
           record key is fempl_id   
           alternate record key is fempl_identreprise with DUPLICATES
           file status is cr_fempl.
       
           select flog assign to "logiciel.dat"
           organization INDEXED
           access mode is dynamic
           record key is flog_cle
           file status is cr_flog.

           select ftick assign to "tickets.dat"
           organization INDEXED
           access mode is dynamic
           record key is ftick_id
           alternate record key is ftick_idcreateur with DUPLICATES
           alternate record key is ftick_idlogiciel with DUPLICATES
           file status is cr_ftick.
      *****************************************************************
       DATA DIVISION.
       FILE SECTION. 
       FD fentr.
       01 tamp_fentr.
           02 fentr_id PIC X(4).
           02 fentr_nom PIC A(30).
           02 fentr_taille PIC A(30).
           02 fentr_statutJur PIC A(30).
           02 fentr_origine PIC A(30).

       FD fempl.
       01 tamp_fempl.
           02 fempl_id PIC X(4).
           02 fempl_identreprise PIC X(4).
           02 fempl_nom PIC A(30).
           02 fempl_prenom PIC A(30).
           02 fempl_numeroTel PIC X(10).

       FD flog.
       01 tamp_flog.
           02 flog_cle.
               03 flog_id PIC X(4).
               03 flog_nom PIC A(30).
           02 flog_nbTickets PIC X(4).

       FD ftick.
       01 tamp_ftick.   
           02 ftick_id PIC X(4).
           02 ftick_idcreateur PIC X(4).
           02 ftick_idlogiciel PIC X(4).
           02 ftick_datecreation PIC 9(08).
           02 ftick_datefermeture PIC 9(08).
           02 ftick_nomMainteneur PIC A(30).

                                                                                

      *****************************************************************
       WORKING-STORAGE SECTION.
       77 Wfin PIC 9(1).
       77 Wtrouve PIC 9(1).
       77 cr_fentr PIC 9(1).
       77 cr_fempl PIC 9(1).
       77 cr_flog PIC 9(1).
       77 cr_ftick PIC 9(1).

      * Récuperer la date courante 
      * Format de la date : AAAAMMJJ
       01 WS-CURRENT-DATE-DATA.
           05  WS-CURRENT-DATE.
               10  WS-CURRENT-YEAR         PIC 9(04).
               10  WS-CURRENT-MONTH        PIC 9(02).
               10  WS-CURRENT-DAY          PIC 9(02).
      * Format de l'heure : HHMM
           05  WS-CURRENT-TIME.
               10  WS-CURRENT-HOURS        PIC 9(02).
               10  WS-CURRENT-MINUTE       PIC 9(02).

       

      *****************************************************************
      ******************************************************************
       PROCEDURE DIVISION.
           MOVE FUNCTION CURRENT-DATE TO WS-CURRENT-DATE-DATA.
           DISPLAY WS-CURRENT-YEAR.

           OPEN I-O fentr
           IF cr_fentr=35 THEN
              OPEN OUTPUT fentr
              DISPLAY "fentr OK"
           END-IF
           CLOSE fentr

           OPEN I-O fempl
           IF cr_fempl=35 THEN
              OPEN OUTPUT fentr
              DISPLAY "fempl OK"
           END-IF
           CLOSE fempl

           OPEN I-O flog
           IF cr_fempl=35 THEN
              OPEN OUTPUT fentr
              DISPLAY "flog OK"
           END-IF
           CLOSE flog

           OPEN I-O ftick
           IF cr_ftick=35 THEN
              OPEN OUTPUT ftick
              DISPLAY "ftick OK"
           END-IF
           CLOSE ftick

      *Utilisation  
           perform AFFICHER_date

           STOP RUN.

           Afficher_date.
           display "current date : " WS-CURRENT-DATE.
           Display "current time : " WS-CURRENT-TIME
           STOP RUN.

     
      * Déclaration fonctions 

      * Prendre en charge un ticket 
      * afficher la liste de tous les tickets, 
      * demander id du ticker voulu
      * Si id inconnu : "ID non existant"
      * Si id connu : "Vous avez bien prit en charge le ticket " 


      *Cloturer ticket 
      * Afficher la liste des tickets que la personne a prit en charge 
      * Si id inconnu : "ID non existant"
      * Si id connu : 
      *    cloture : datecloture <- date()

      

      * Supprimer un ticket 
      * supprime les tickets ayant une date de cloture > a 1 an  
      * date cloture > current date - 1 an
      * current_date - date_cloture > 00010000