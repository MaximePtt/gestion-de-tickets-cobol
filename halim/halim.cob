*****************************************************************
      * Program name:    projet
      * Original author: halim el ouatani
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. gesticksHalim.
       AUTHOR. HALIM EL OUATANI .
      *****************************************************************
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

           select fentr assign to "entreprise.dat"
           organization SEQUENTIAL
           access mode is DYNAMIC
           file status is cr_fentr.

           

           select fempl assign to "employes.dat"
           organization indexed
           access mode is dynamic
           record key is fempl_id
           alternate record key is fempl_identreprise with DUPLICATES
           file status is cr_fempl.

           select flog assign to "logiciel.dat"
           organization INDEXED
           access mode is dynamic
           record key is flog_cle
           file status is cr_flog.

           select ftick assign to "tickets.dat"
           organization INDEXED
           access mode is dynamic
           record key is ftick_id
           alternate record key is ftick_idcreateur with DUPLICATES
           alternate record key is ftick_idlogiciel with DUPLICATES
           file status is cr_ftick.
      *****************************************************************
       DATA DIVISION.
       FILE SECTION.
       FD fentr.
       01 tamp_fentr.
           02 fentr_id PIC X(4).
           02 fentr_nom PIC A(30).
           02 fentr_taille PIC A(30).
           02 fentr_statutJur PIC A(30).
           02 fentr_origine PIC A(30).

       FD fempl.
       01 tamp_fempl.
           02 fempl_id PIC X(4).
           02 fempl_identreprise PIC X(4).
           02 fempl_nom PIC A(30).
           02 fempl_prenom PIC A(30).
           02 fempl_numeroTel PIC X(10).

       FD flog.
       01 tamp_flog.
           02 flog_cle.
               03 flog_id PIC X(4).
               03 flog_nom PIC A(30).
           02 flog_nbTickets PIC X(4).

       FD ftick.
       01 tamp_ftick.
           02 ftick_id PIC X(4).
           02 ftick_idcreateur PIC X(4).
           02 ftick_idlogiciel PIC X(4).
           02 ftick_datecreation PIC A(30).
           02 ftick_datefermeture PIC A(30).
           02 ftick_nomMainteneur PIC A(30).




      *****************************************************************
       WORKING-STORAGE SECTION.
       77 Wfin PIC 9(1).
       77 Wtrouve PIC 9(1).
       77 cr_fentr PIC 9(1).
       77 cr_fempl PIC 9(1).
       77 cr_flog PIC 9(1).
       77 cr_ftick PIC 9(1).
       77 wrep
     


       



       77 sortie_EXISTE_LOGICIEL_cle PIC 9(1).
       01 param_EXISTE_LOGICIEL_cle.
               03 wflog_id PIC X(4).
               03 wflog_nom PIC A(30).


      *****************************************************************
      ******************************************************************
       PROCEDURE DIVISION.

           OPEN I-O fentr
           IF cr_fentr=35 THEN
              OPEN OUTPUT fentr
              DISPLAY "fentr OK"
           END-IF
           CLOSE fentr

           OPEN I-O fempl
           IF cr_fempl=35 THEN
              OPEN OUTPUT fentr
              DISPLAY "fempl OK"
           END-IF
           CLOSE fempl

           OPEN I-O flog
           IF cr_fempl=35 THEN
              OPEN OUTPUT fentr
              DISPLAY "flog OK"
           END-IF
           CLOSE flog

           OPEN I-O ftick
           IF cr_ftick=35 THEN
              OPEN OUTPUT ftick
              DISPLAY "ftick OK"
           END-IF
           CLOSE ftick

           PERFORM EXISTE_LOGICIEL_CLE
           PERFORM AJOUT_LOGICIEL
           PERFORM MODIFIER_LOGICIEL 

           STOP RUN.


           EXISTE_LOGICIEL_CLE.
           MOVE 0 to sortie_EXISTE_LOGICIEL_CLE
           MOVE param_EXISTE_LOGICIEL_cle TO flog_cle
           OPEN INPUT flog
              READ flog INTO tamp_flog
                 KEY IS flog_cle
                 NOT INVALID KEY
                    MOVE 1 TO sortie_EXISTE_LOGICIEL_CLE
              END-READ
           CLOSE flog .
            
           AJOUT_LOGICIEL.
           Display "-AJOUTER_logiciel"
           PERFORM WITH TEST AFTER UNTIL SORTIE_EXISTE_LOGICIEL_CLE =0  
             DISPLAY  '__Entrer l identifiant du nouveau logiciel   : '
             accept flog_id
             DISPLAY '__Entrer le  nom du nouvau logiciel:'
             accept flog_nom 
             Move flog_id to  wflog_id
             Move flog_nom to wflog_nom 
             Move flog_cle to param_EXISTE_LOGICIEL_cle
             PERFORM EXISTE_LOGICIEL_CLE 
           END-PERFORM 
       *> Le nombre de tickets sera initialiser à 0 
             Move 0 to flog_nbTickets   .


           MODIFIER_LOGICIEL.
            
           PERFORM WITH TEST AFTER UNTIL wrep = 0
         

           
