      *****************************************************************
      * Program name:    TP1                               
      * Original author: Maxime Potet                                           
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. gestionElections.
       AUTHOR. Maxime Potet.
      *****************************************************************
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION.
       FILE-CONTROL. 
           SELECT fbu ASSIGN  TO "bureaux.dat"
           organization indexed
           access mode is dynamic
           record key is fbu_code
           alternate record key is fbu_ville
           alternate record key is fbu_region WITH DUPLICATES
           file status is cr_fbu.

           SELECT fv ASSIGN TO "votants.dat"
           organization indexed
           access mode is dynamic 
           record key is fv_idE
           alternate record key is fv_idprocu
           alternate record key is fv_code WITH DUPLICATES
           file status is cr_fv.

           SELECT fres ASSIGN TO "resultats.dat"
           organization indexed
           access mode is dynamic
           record key is fres_code
           file status is cr_fres.

           SELECT fe ASSIGN TO "electeurs.dat"
           organization sequential
           access mode is sequential
           file status is cr_fe.

           SELECT feage ASSIGN TO "electeursAges.dat"
           organization sequential
           access mode is sequential
           file status is cr_feage.
      *****************************************************************
       DATA DIVISION.
       FILE SECTION. 
       FD fbu.
       01 tamp_fbu.
           02 fbu_code PIC X(4).
           02 fbu_ville PIC A(30).
           02 fbu_heure_fermeture PIC 9(2).
           02 fbu_heure_ouverture PIC 9(2).
           02 fbu_region PIC 9(2).
       FD fv.
       01 tamp_fv.
           02 fv_idE PIC 9(15).
           02 fv_heure PIC 9(2).
           02 fv_minute PIC 9(2).
           02 fv_idprocu PIC 9(15).
           02 fv_code PIC X(4).
       FD fres.
       01 tamp_fres.
           02 fres_code PIC X(4).
           02 fres_nb1 PIC 9(12).
           02 fres_nb2 PIC 9(12).
           02 fres_nbblanc PIC 9(12).
       FD fe.
       01 tamp_fe.
           02 fe_idE PIC 9(15).
           02 fe_nom PIC A(30).
           02 fe_prenom PIC A(30).
           02 fe_age PIC 9(3).
           02 fe_villeHabitation PIC A(30).
       FD feage.
       01 tamp_feage.
           02 feage_nom PIC A(30).
           02 feage_prenom PIC A(30).
           02 feage_villeHabitation PIC A(30).
      *****************************************************************
       WORKING-STORAGE SECTION.
       77 cr_fbu PIC 9(2).
       77 cr_fv PIC 9(2).
       77 cr_fres PIC 9(2).
       77 cr_fe PIC 9(2).
       77 cr_feage PIC 9(2).

       77 Wfin PIC 9(1).
       77 Wtrouve PIC 9(1).

       77 Wfe_idE PIC 9(15).
       77 Wfe_nom PIC A(30).
       77 Wfe_prenom PIC A(30).

       77 param_EXISTE_ELECTEUR_idE PIC 9(15).
       77 sortie_EXISTE_ELECTEUR PIC 9(1).

       77 Wfbu_code PIC X(4).
       77 Wfbu_ville PIC A(30).

       77 param_EXISTE_BUREAU_VILLE_ville PIC A(30).
       77 sortie_EXISTE_BUREAU_VILLE PIC 9(1).

       77 param_EXISTE_BUREAU_CODE_code PIC X(4).
       77 sortie_EXISTE_BUREAU_CODE PIC 9(1).
      *****************************************************************
      ******************************************************************
       PROCEDURE DIVISION.
           OPEN I-O fbu
           IF cr_fbu=35 THEN
              OPEN OUTPUT fbu
              DISPLAY "fbu OK"
           END-IF
           CLOSE fbu

           OPEN I-O fv
           IF cr_fv=35 THEN
              OPEN OUTPUT fv
              DISPLAY "fv OK"
           END-IF
           CLOSE fv

           OPEN I-O fres
           IF cr_fres=35 THEN
              OPEN OUTPUT fres
              DISPLAY "fres OK"
           END-IF
           CLOSE fres

           OPEN I-O fe
           IF cr_fe=35 THEN
              OPEN OUTPUT fe
              DISPLAY "fe OK"
           END-IF
           CLOSE fe

           OPEN I-O feage
           IF cr_feage=35 THEN
              OPEN OUTPUT feage
              DISPLAY "feage OK"
           END-IF
           CLOSE feage


           PERFORM AFFICHER_BUREAUX
           PERFORM AJOUTER_BUREAU
           PERFORM AFFICHER_BUREAUX


           STOP RUN.
      * ELECTEURS
           AJOUTER_ELECTEUR.
           DISPLAY "- AJOUTER_ELECTEUR -"
           PERFORM WITH TEST AFTER UNTIL sortie_EXISTE_ELECTEUR = 0
              DISPLAY "ID :"
              ACCEPT fe_idE
              MOVE fe_idE to Wfe_idE
              MOVE fe_idE to param_EXISTE_ELECTEUR_idE
              PERFORM EXISTE_ELECTEUR
           END-PERFORM
              DISPLAY "Nom :"
              ACCEPT fe_nom
              DISPLAY "Prenom :"
              ACCEPT fe_prenom
           PERFORM WITH TEST AFTER UNTIL fe_age >= 18 
              DISPLAY "Age :"
              ACCEPT fe_age
           END-PERFORM
           DISPLAY "Ville Habitation :"
           ACCEPT fe_villeHabitation
           MOVE Wfe_idE to fe_idE
           OPEN EXTEND fe
           WRITE tamp_fe
           END-WRITE
           CLOSE fe.

           AFFICHER_ELECTEURS.
           DISPLAY "- AFFICHER_ELECTEURS -"
           OPEN INPUT fe
           MOVE 0 TO Wfin 
           PERFORM WITH TEST AFTER UNTIL Wfin = 1
              READ fe
              AT END
                 MOVE 1 TO Wfin
              NOT AT END
                 DISPLAY fe_idE
                 DISPLAY fe_nom
                 DISPLAY fe_prenom 
                 DISPLAY fe_age 
                 DISPLAY fe_villeHabitation 
                 DISPLAY " "
              END-READ
           END-PERFORM
           CLOSE fe.

           EXISTE_ELECTEUR.
           MOVE 0 to sortie_EXISTE_ELECTEUR
           MOVE 0 TO Wfin
           MOVE 0 TO Wtrouve
           OPEN INPUT fe
           PERFORM WITH TEST AFTER UNTIL Wfin = 1 OR Wtrouve = 1
              READ fe
              AT END
                 MOVE 1 TO Wfin
              NOT AT END
                 IF fe_idE = param_EXISTE_ELECTEUR_idE THEN
                    MOVE 1 TO Wtrouve
                    MOVE 1 TO sortie_EXISTE_ELECTEUR
                 END-IF
              END-READ
           END-PERFORM
           CLOSE fe.

           RECHERCHER_INFOS_ELECTEUR.
           DISPLAY "- RECHERCHER_INFOS_ELECTEUR -"
           MOVE 0 TO Wfin
           MOVE 0 TO Wtrouve
           DISPLAY "NOM :"
           ACCEPT Wfe_nom
           DISPLAY "PRENOM :"
           ACCEPT Wfe_prenom
           OPEN INPUT fe
           PERFORM WITH TEST AFTER UNTIL Wfin = 1 OR Wtrouve = 1
              READ fe
              AT END
                 MOVE 1 TO Wfin
                 DISPLAY "NOM et PRENOM non trouvés"
              NOT AT END
                 IF fe_nom = Wfe_nom AND fe_prenom = Wfe_prenom THEN
                    MOVE 1 to Wtrouve
                    DISPLAY "IDENTIFIANT :"
                    DISPLAY fe_idE
                    DISPLAY "VILLE :"
                    DISPLAY fe_villeHabitation
                 END-IF
              END-READ
           END-PERFORM

           CLOSE fe.

      * ELECTEURS AGES
           AFFICHER_ELECTEURS_AGES.
           DISPLAY "- AFFICHER_ELECTEURS AGES -"
           OPEN INPUT feage
           MOVE 0 TO Wfin 
           PERFORM WITH TEST AFTER UNTIL Wfin = 1
              READ feage
              AT END
                 MOVE 1 TO Wfin
              NOT AT END
                 DISPLAY feage_nom
                 DISPLAY feage_prenom
                 DISPLAY feage_villeHabitation
                 DISPLAY " "
              END-READ
           END-PERFORM
           CLOSE feage.
           
           MAJ_ELECTEURS_AGES.
           DISPLAY "- MAJ_ELECTEURS_AGES -"
           OPEN OUTPUT feage
           CLOSE feage
           OPEN INPUT fe
           OPEN EXTEND feage
           MOVE 0 TO Wfin 
           PERFORM WITH TEST AFTER UNTIL Wfin = 1
              READ fe
              AT END
                 MOVE 1 TO Wfin
              NOT AT END
                 IF fe_age > 80 THEN
                    MOVE fe_nom TO feage_nom
                    MOVE fe_prenom TO feage_prenom
                    MOVE fe_villehabitation TO feage_villehabitation
                    WRITE tamp_feage
                 END-IF
              END-READ
           END-PERFORM
           CLOSE feage
           CLOSE fe.

      * BUREAUX
           AFFICHER_BUREAUX.
           DISPLAY "- AFFICHER_BUREAUX -"
           OPEN INPUT fbu
           MOVE 0 TO Wfin 
           PERFORM WITH TEST AFTER UNTIL Wfin = 1
              READ fbu
              AT END
                 MOVE 1 TO Wfin
              NOT AT END
                 DISPLAY fbu_code
                 DISPLAY fbu_region
                 DISPLAY fbu_ville
                 DISPLAY fbu_heure_ouverture
                 DISPLAY fbu_heure_fermeture
                 DISPLAY " "
              END-READ
           END-PERFORM
           CLOSE fbu.
           
           AJOUTER_BUREAU.
           DISPLAY "- AJOUTER_BUREAU -"
           PERFORM WITH TEST AFTER UNTIL sortie_EXISTE_BUREAU_CODE = 0
              DISPLAY "Code :"
              ACCEPT fbu_code
              MOVE fbu_code to Wfbu_code
              MOVE fbu_code to param_EXISTE_BUREAU_CODE_code
              PERFORM EXISTE_BUREAU_CODE
           END-PERFORM
           PERFORM WITH TEST AFTER UNTIL sortie_EXISTE_BUREAU_VILLE = 0
              DISPLAY "Ville :"
              ACCEPT fbu_ville
              MOVE fbu_ville to Wfbu_ville
              MOVE fbu_ville to param_EXISTE_BUREAU_VILLE_ville
              PERFORM EXISTE_BUREAU_VILLE
           END-PERFORM
           DISPLAY "Heure ouverture :"
           ACCEPT fbu_heure_ouverture
           DISPLAY "Heure fermeture :"
           ACCEPT fbu_heure_fermeture
           DISPLAY "Région :"
           ACCEPT fbu_region
           MOVE Wfbu_code to fbu_code
           MOVE Wfbu_ville to fbu_ville
           OPEN I-O fbu
              WRITE tamp_fbu
                 
              END-WRITE
           CLOSE fbu.

           EXISTE_BUREAU_CODE.
           MOVE 0 to sortie_EXISTE_BUREAU_CODE
           MOVE param_EXISTE_BUREAU_CODE_code TO fbu_code
           OPEN INPUT fbu
              READ fbu INTO tamp_fbu
                 KEY IS fbu_code
                 NOT INVALID KEY
                    MOVE 1 TO sortie_EXISTE_BUREAU_CODE
              END-READ
           CLOSE fbu.

           EXISTE_BUREAU_VILLE.
           MOVE 0 to sortie_EXISTE_BUREAU_VILLE
           MOVE param_EXISTE_BUREAU_VILLE_ville TO fbu_ville
           OPEN INPUT fbu
              READ fbu INTO tamp_fbu
                 KEY IS fbu_ville
                 NOT INVALID KEY
                    MOVE 1 TO sortie_EXISTE_BUREAU_VILLE
              END-READ
           CLOSE fbu.
