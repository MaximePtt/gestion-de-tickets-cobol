      *****************************************************************
      * Program name:    Gesticks
      * Original author: POTET Maxime, EL OUATANI Halim, VATIN Alexis
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. gesticks.
       AUTHOR. POTET Maxime, EL OUATANI Halim, VATIN Alexis.
      *****************************************************************
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           select fentr assign to "entreprises.dat"
           organization SEQUENTIAL
           access mode is sequential
           file status is cr_fentr.

           select fempl assign to "employes.dat"
           organization indexed
           access mode is dynamic
           record key is fempl_id
           alternate record key is fempl_identreprise with DUPLICATES
           alternate record key is fempl_nom with DUPLICATES
           file status is cr_fempl.

           select flog assign to "logiciels.dat"
           organization INDEXED
           access mode is dynamic
           record key is flog_id
           alternate record key is flog_nom
           file status is cr_flog.

           select ftick assign to "tickets.dat"
           organization INDEXED
           access mode is dynamic
           record key is ftick_id
           alternate record key is ftick_idcreateur with DUPLICATES
           alternate record key is ftick_idlogiciel with DUPLICATES
           file status is cr_ftick.
      *****************************************************************
       DATA DIVISION.
       FILE SECTION.
       FD fentr.
       01 tamp_fentr.
           02 fentr_id PIC 9(4).
           02 fentr_nom PIC A(30).
           02 fentr_taille PIC A(1).
           02 fentr_statutJur PIC A(30).
           02 fentr_origine PIC A(1).

       FD fempl.
       01 tamp_fempl.
           02 fempl_id PIC 9(4).
           02 fempl_identreprise PIC 9(4).
           02 fempl_nom PIC A(30).
           02 fempl_prenom PIC A(30).
           02 fempl_numeroTel PIC 9(10).

       FD flog.
       01 tamp_flog.
           02 flog_id PIC 9(4).
           02 flog_nom PIC X(30).
           02 flog_nbTickets PIC 9(4).
           02 flog_typelogiciel PIC 9(2).

       FD ftick.
       01 tamp_ftick.
           02 ftick_id PIC 9(4).
           02 ftick_idcreateur PIC 9(4).
           02 ftick_idlogiciel PIC 9(4).
           02 ftick_description PIC A(50).
           02 ftick_datecreation PIC 9(08).
           02 ftick_datefermeture PIC 9(08).
           02 ftick_nomMainteneur PIC A(30).
      *****************************************************************
       WORKING-STORAGE SECTION.
      * Récuperer la date courante
      * Format de la date : AAAAMMJJ
       01 WS-CURRENT-DATE-DATA.
           05  WS-CURRENT-DATE.
               10  WS-CURRENT-YEAR         PIC 9(04).
               10  WS-CURRENT-MONTH        PIC 9(02).
               10  WS-CURRENT-DAY          PIC 9(02).
      * Format de l'heure : HHMM
           05  WS-CURRENT-TIME.
               10  WS-CURRENT-HOURS        PIC 9(02).
               10  WS-CURRENT-MINUTE       PIC 9(02).

       77 cr_fentr PIC 9(1).
       77 cr_fempl PIC 9(1).
       77 cr_flog PIC 9(1).
       77 cr_ftick PIC 9(1).
       77 Wfin PIC 9(1).
       77 Wfdz PIC 9(1).
       77 Wtrouve PIC 9(1).
       77 WreponseAccept PIC X(9).

       77 Wfentr_nom PIC A(30).
       77 Wfentr_taille PIC A(1).
       77 Wfentr_statutJur PIC A(30).
       77 Wfentr_origine PIC A(1).

       77 Wflog_typelogiciel PIC 9(1).

       77 adminPassword PIC X(9).

       77 param_DISPLAY_CLIENT_MENU_nomEntreprise PIC A(30).
       77 param_DISPLAY_CLIENT_MENU_nomEmploye PIC A(30).
       77 param_DISPLAY_CLIENT_MENU_prenomEmploye PIC A(30).

       77 param_EXISTE_ENTREPRISE_nom PIC A(30).
       77 sortie_EXISTE_ENTREPRISE_existe PIC 9(1).

       77 sortie_GET_NEW_ID_ENTREPRISE_id PIC 9(4).

       77 param_CREATE_EMPLOYE_IF_NOT_EXIST_nom PIC A(30).
       77 param_CREATE_EMPLOYE_IF_NOT_EXIST_prenom PIC A(30).
       77 sortie_CREATE_EMPLOYE_IF_NOT_EXIST_id PIC 9(4).

       77 param_EXISTE_EMPLOYE_nom PIC A(30).
       77 param_EXISTE_EMPLOYE_prenom PIC A(30).
       77 param_EXISTE_EMPLOYE_idEntr PIC 9(4).
       77 sortie_EXISTE_EMPLOYE_existe PIC 9(1).
       77 sortie_EXISTE_EMPLOYE_idEmpl PIC 9(4).

       77 param_GET_ID_ENTREPRISE_nom PIC A(30).
       77 sortie_GET_ID_ENTREPRISE_id PIC 9(4).

       77 sortie_GET_NEW_ID_TICKET_id PIC 9(4).

       77 sortie_GET_NEW_ID_EMPLOYE_id PIC 9(4).

       77 param_EXISTE_LOGICIEL_nom PIC X(30).
       77 sortie_EXISTE_LOGICIEL_id PIC 9(4).
       77 sortie_EXISTE_LOGICIEL_existe PIC 9(1).

       77 entree_AFFICHER_TICKETS_EN_CHARGE_nomMainteneur PIC A(30).

       77 entree_DISPLAY_ADMIN_MENU_nomMainteneur PIC A(30).

       77 param_EXISTE_TICKET_id PIC 9(4).
       77 sortie_EXISTE_TICKET_existe PIC 9(1).

       77 param_AFFICHER_TICKETS_EMPLOYE_idCreateur PIC 9(4).

       77 sortie_GET_NEW_ID_LOGICIEL_id PIC 9(4).

       77 param_INCR_NB_TICKETS_LOGICIEL_idLog PIC 9(4).

       77 param_AFFICHER_NB_TICKETS_LOGICIEL_idLog PIC 9(4).

       77 param_SUB_NB_TICKETS_LOGICIEL_idLog PIC 9(4).

      *****************************************************************
      ******************************************************************
       PROCEDURE DIVISION.
           OPEN I-O fentr
           IF cr_fentr <> 0 THEN
              OPEN OUTPUT fentr
              DISPLAY "fentr OK"
           END-IF
           CLOSE fentr

           OPEN I-O fempl
           IF cr_fempl <> 0 THEN
              OPEN OUTPUT fempl
              DISPLAY "fempl OK"
           END-IF
           CLOSE fempl

           OPEN I-O flog
           IF cr_flog <> 0 THEN
              OPEN OUTPUT flog
              DISPLAY "flog OK"
           END-IF
           CLOSE flog

           OPEN I-O ftick
           IF cr_ftick <> 0 THEN
              OPEN OUTPUT ftick
              DISPLAY "ftick OK"
           END-IF
           CLOSE ftick

           DISPLAY " "
           PERFORM DISPLAY_WELCOME_MESSAGE
           DISPLAY " "
           PERFORM DISPLAY_MAIN_MENU

           STOP RUN.

           DISPLAY_WELCOME_MESSAGE.
           DISPLAY "-=- -=-=-    -=-=- -=-"
           DISPLAY " -=-=- GESTICKS -=-=- "
           DISPLAY "-=-  -=-=-  -=-=-  -=-".

      ** MENUS
           DISPLAY_MAIN_MENU.
               DISPLAY "| MAIN MENU |"
               DISPLAY "<- 0 : Quitter"
               DISPLAY " - 1 : Connexion administrateur"
               DISPLAY " - 2 : Connexion client"
               DISPLAY "Entrez une réponse : "
               ACCEPT WreponseAccept
               PERFORM WITH TEST BEFORE UNTIL WreponseAccept = 1
               OR WreponseAccept = 2 OR WreponseAccept = 0
                   DISPLAY "Réponse incorrecte, réessayez"
                   ACCEPT WreponseAccept
               END-PERFORM
               IF WreponseAccept = 1
                   DISPLAY " "
                   PERFORM ADMIN_CONNECTION
               ELSE
                   IF WreponseAccept = 2
                       DISPLAY " "
                       PERFORM AFFICHER_ENTREPRISES
                       DISPLAY " "
                       PERFORM CLIENT_CONNECTION
                   ELSE
                       IF WreponseAccept = 0
                           DISPLAY " "
                           DISPLAY "Logiciel quitté"
                           DISPLAY "A Bientot.."
                           EXIT
                       END-IF
                   END-IF
               END-IF.

           ADMIN_CONNECTION.
               MOVE "password" TO adminPassword
               
               DISPLAY "| ADMIN CONNECTION |"
               DISPLAY " "
               DISPLAY "Entrez le mot de passe administrateur :"
               ACCEPT WreponseAccept
               IF WreponseAccept <> adminPassword
                  DISPLAY " "
                  DISPLAY
                  "/!\ Mot de passe incorrect, retour au menu principal"
                  DISPLAY " "
                  PERFORM DISPLAY_MAIN_MENU
               ELSE
                   DISPLAY " "
                   DISPLAY "Entrez votre nom d'administrateur :"
                   ACCEPT entree_DISPLAY_ADMIN_MENU_nomMainteneur
                   MOVE Function
                   Upper-case(entree_DISPLAY_ADMIN_MENU_nomMainteneur)
                   TO entree_DISPLAY_ADMIN_MENU_nomMainteneur
                   DISPLAY " "
                   DISPLAY "Connecté en tant que "
                   entree_DISPLAY_ADMIN_MENU_nomMainteneur
                   PERFORM DISPLAY_ADMIN_MENU
               END-IF.

      * params : nomMainteneur
           DISPLAY_ADMIN_MENU.
               DISPLAY "| ADMIN MENU |"
               DISPLAY "<- 0 : Retour Menu Principal"
               DISPLAY " - 1 : Gérer tickets"
               DISPLAY " - 2 : Gérer logiciels"
               DISPLAY " - 3 : Gérer entreprises"
               DISPLAY " - 4 : Statistiques"
               DISPLAY "Entrez une réponse : "
               ACCEPT WreponseAccept
               PERFORM WITH TEST BEFORE UNTIL WreponseAccept = 0
               OR WreponseAccept = 1 OR WreponseAccept = 2
               OR WreponseAccept = 3 OR WreponseAccept = 4
                   DISPLAY "Réponse incorrecte, réessayez"
                   ACCEPT WreponseAccept
               END-PERFORM
               IF WreponseAccept = 0
                   DISPLAY " "
                   DISPLAY "Déconnexion de l'admin "
                   entree_DISPLAY_ADMIN_MENU_nomMainteneur 
                   DISPLAY " "
                   PERFORM DISPLAY_MAIN_MENU
               ELSE
                   IF WreponseAccept = 1
                       DISPLAY " "
                       PERFORM DISPLAY_GERER_TICKETS
                   ELSE
                       IF WreponseAccept = 2
                           DISPLAY " "
                           PERFORM DISPLAY_GERER_LOGICIELS
                       ELSE
                           IF WreponseAccept = 3
                               DISPLAY " "
                               PERFORM DISPLAY_GERER_ENTREPRISES
                           ELSE
                               IF WreponseAccept = 4
                                   DISPLAY " "
                                   PERFORM DISPLAY_STATS_MENU
                               END-IF
                           END-IF
                       END-IF
                   END-IF
               END-IF.

           CLIENT_CONNECTION.
               DISPLAY "| CLIENT CONNECTION |"
               DISPLAY "Entrez le nom de votre entreprise :"
               ACCEPT param_EXISTE_ENTREPRISE_nom
               MOVE Function Upper-case(param_EXISTE_ENTREPRISE_nom)
               TO param_EXISTE_ENTREPRISE_nom
               PERFORM EXISTE_ENTREPRISE
               IF sortie_EXISTE_ENTREPRISE_existe = 1
                   DISPLAY "Entrez votre nom :"
                   ACCEPT param_CREATE_EMPLOYE_IF_NOT_EXIST_nom
                   MOVE Function
                   Upper-case(param_CREATE_EMPLOYE_IF_NOT_EXIST_nom)
                   TO param_CREATE_EMPLOYE_IF_NOT_EXIST_nom
                   DISPLAY "Entrez votre prénom :"
                   ACCEPT param_CREATE_EMPLOYE_IF_NOT_EXIST_prenom
                   MOVE Function
                   Upper-case(param_CREATE_EMPLOYE_IF_NOT_EXIST_prenom)
                   TO param_CREATE_EMPLOYE_IF_NOT_EXIST_prenom
                   PERFORM CREATE_EMPLOYE_IF_NOT_EXIST
                   MOVE param_CREATE_EMPLOYE_IF_NOT_EXIST_nom
                   TO param_DISPLAY_CLIENT_MENU_nomEmploye
                   MOVE param_CREATE_EMPLOYE_IF_NOT_EXIST_prenom
                   TO param_DISPLAY_CLIENT_MENU_prenomEmploye
                   DISPLAY " "
                   DISPLAY "Vous etes connecté en tant que "
                   param_DISPLAY_CLIENT_MENU_prenomEmploye
                   DISPLAY " "
                   PERFORM DISPLAY_CLIENT_MENU
               ELSE
                  DISPLAY " "
                  DISPLAY "/!\ Nom d'entreprise inconnu"
                  DISPLAY
                  "Contactez un admin pour ajouter votre entreprise"
                  DISPLAY " "
                  PERFORM DISPLAY_MAIN_MENU
               END-IF.

      * params : nomEntreprise, nomEmploye, prenomEmploye
           DISPLAY_CLIENT_MENU.
               DISPLAY "| CLIENT MENU |"
               DISPLAY "<- 0 : Retour Menu Principal"
               DISPLAY " - 1 : Menu Lister Tickets"
               DISPLAY " - 2 : Créer un ticket"
               DISPLAY " - 3 : Supprimer un ticket"
               DISPLAY "Entrez une réponse : "
               ACCEPT WreponseAccept
               PERFORM WITH TEST BEFORE UNTIL WreponseAccept = 0
               OR WreponseAccept = 1 OR WreponseAccept = 2
               OR WreponseAccept = 3
                   DISPLAY "Réponse incorrecte, réessayez"
                   ACCEPT WreponseAccept
               END-PERFORM
               IF WreponseAccept = 0
                   DISPLAY " "
                   DISPLAY "Déconnexion.."
                   DISPLAY "Au revoir "
                   param_DISPLAY_CLIENT_MENU_prenomEmploye
                   DISPLAY " "
                   PERFORM DISPLAY_MAIN_MENU
               ELSE
                   IF WreponseAccept = 1
                       MOVE sortie_CREATE_EMPLOYE_IF_NOT_EXIST_id
                       TO param_AFFICHER_TICKETS_EMPLOYE_idCreateur
                       DISPLAY " "
                       PERFORM DISPLAY_LISTER_TICKETS_MENU
                   ELSE
                       IF WreponseAccept = 2
                           DISPLAY " "
                           PERFORM AFFICHER_LOGICIELS
                           DISPLAY " "
                           PERFORM AJOUTER_TICKET
                           DISPLAY " "
                           PERFORM DISPLAY_CLIENT_MENU
                       ELSE
                           IF WreponseAccept = 3
                              MOVE sortie_CREATE_EMPLOYE_IF_NOT_EXIST_id
                            TO param_AFFICHER_TICKETS_EMPLOYE_idCreateur
                               DISPLAY " "
                               PERFORM AFFICHER_TICKETS_EMPLOYE
                               DISPLAY " "
                               PERFORM SUPPRIMER_TICKET
                               DISPLAY " "
                               PERFORM DISPLAY_CLIENT_MENU
                           END-IF
                       END-IF
                   END-IF
               END-IF.

           DISPLAY_GERER_TICKETS.
               DISPLAY "| GESTION DES TICKETS |"
               DISPLAY "<- 0 : Retour Menu Admin"
               DISPLAY " - 1 : Lister tickets disponibles"
               DISPLAY " - 2 : Prendre en charge un ticket"
               DISPLAY " - 3 : Clore un ticket"
               DISPLAY "Entrez une réponse : "
               ACCEPT WreponseAccept
               PERFORM WITH TEST BEFORE UNTIL WreponseAccept = 0
               OR WreponseAccept = 1 OR WreponseAccept = 2
               OR WreponseAccept = 3
                   DISPLAY "Réponse incorrecte, réessayez"
                   ACCEPT WreponseAccept
               END-PERFORM
               IF WreponseAccept = 0
                   DISPLAY " "
                   PERFORM DISPLAY_ADMIN_MENU
               ELSE
                   IF WreponseAccept = 1
                       DISPLAY " "
                       PERFORM AFFICHER_TICKETS_DISPONIBLES
                       DISPLAY " "
                       PERFORM DISPLAY_GERER_TICKETS
                   ELSE
                       IF WreponseAccept = 2
                           DISPLAY " "
                           PERFORM AFFICHER_TICKETS_DISPONIBLES
                           DISPLAY " "
                           PERFORM PRENDRE_EN_CHARGE_TICKET
                           DISPLAY " "
                           PERFORM DISPLAY_GERER_TICKETS
                       ELSE
                           IF WreponseAccept = 3
                            MOVE entree_DISPLAY_ADMIN_MENU_nomMainteneur
                      TO entree_AFFICHER_TICKETS_EN_CHARGE_nomMainteneur
                               DISPLAY " "
                               PERFORM AFFICHER_TICKETS_EN_CHARGE
                               DISPLAY " "
                               PERFORM CLORE_TICKET
                               DISPLAY " "
                               PERFORM DISPLAY_GERER_TICKETS
                           END-IF
                       END-IF
                   END-IF
               END-IF.

           DISPLAY_LISTER_TICKETS_MENU.
               DISPLAY "| LISTER TICKETS MENU |"
               DISPLAY "<- 0 : Retour Menu Client"
               DISPLAY " - 1 : Lister tous vos tickets"
               DISPLAY " - 2 : Lister vos tickets sur un logiciel"
               DISPLAY "Entrez une réponse : "
               ACCEPT WreponseAccept
               PERFORM WITH TEST BEFORE UNTIL WreponseAccept = 0
               OR WreponseAccept = 1 OR WreponseAccept = 2
                   DISPLAY "Réponse incorrecte, réessayez"
                   ACCEPT WreponseAccept
               END-PERFORM
               IF WreponseAccept = 0
                   DISPLAY " "
                   PERFORM DISPLAY_CLIENT_MENU
               ELSE
                   IF WreponseAccept = 1
                       DISPLAY " "
                       PERFORM AFFICHER_TICKETS_EMPLOYE
                       DISPLAY " "
                       PERFORM DISPLAY_LISTER_TICKETS_MENU
                   ELSE
                       IF WreponseAccept = 2
                           DISPLAY " "
                           PERFORM AFFICHER_LOGICIELS
                           DISPLAY " "
                           PERFORM AFFICHER_TICKETS_EMPLOYE_LOGICIEL
                           DISPLAY " "
                           PERFORM DISPLAY_LISTER_TICKETS_MENU
                       END-IF
                   END-IF
               END-IF.

           DISPLAY_STATS_MENU.
               DISPLAY "| STATISTIQUES |"
               DISPLAY "<- 0 : Retour Menu Admin"
               DISPLAY " - 1 : Nb tickets pour un logiciel"
               DISPLAY " - 2 : Lister tous les employés utilisateurs"
               DISPLAY " - 3 : Lister tous les tickets créés"
               DISPLAY "Entrez une réponse : "
               ACCEPT WreponseAccept
               PERFORM WITH TEST BEFORE UNTIL WreponseAccept = 0
               OR WreponseAccept = 1 OR WreponseAccept = 2
               OR WreponseAccept = 3
                   DISPLAY "Réponse incorrecte, réessayez"
                   ACCEPT WreponseAccept
               END-PERFORM
               IF WreponseAccept = 0
                   DISPLAY " "
                   PERFORM DISPLAY_ADMIN_MENU
               ELSE
                   IF WreponseAccept = 1
                       DISPLAY " "
                       PERFORM AFFICHER_LOGICIELS
                       DISPLAY " "
                       PERFORM AFFICHER_NB_TICKETS_LOGICIEL
                       DISPLAY " "
                       PERFORM DISPLAY_STATS_MENU
                   ELSE
                       IF WreponseAccept = 2
                           DISPLAY " "
                           PERFORM AFFICHER_EMPLOYES
                           DISPLAY " "
                           PERFORM DISPLAY_STATS_MENU
                       ELSE
                           IF WreponseAccept = 3
                               DISPLAY " "
                               PERFORM AFFICHER_TOUS_TICKETS
                               DISPLAY " "
                               PERFORM DISPLAY_STATS_MENU
                           END-IF
                       END-IF
                   END-IF
               END-IF.

           DISPLAY_GERER_LOGICIELS.
               DISPLAY "| GESTION DES LOGICIELS |"
               DISPLAY "<- 0 : Retour Menu Admin"
               DISPLAY " - 1 : Ajouter un logiciel"
               DISPLAY " - 2 : Lister les logiciels"
               DISPLAY "Entrez une réponse : "
               ACCEPT WreponseAccept
               PERFORM WITH TEST BEFORE UNTIL WreponseAccept = 0
               OR WreponseAccept = 1 OR WreponseAccept = 2
                   DISPLAY "Réponse incorrecte, réessayez"
                   ACCEPT WreponseAccept
               END-PERFORM
               IF WreponseAccept = 0
                   DISPLAY " "
                   PERFORM DISPLAY_ADMIN_MENU
               ELSE
                   IF WreponseAccept = 1
                       DISPLAY " "
                       PERFORM AFFICHER_LOGICIELS
                       DISPLAY " "
                       PERFORM AJOUTER_LOGICIEL
                       DISPLAY " "
                       PERFORM DISPLAY_GERER_LOGICIELS
                   ELSE 
                       IF WreponseAccept = 2
                           DISPLAY " "
                           PERFORM AFFICHER_LOGICIELS
                           DISPLAY " "
                           PERFORM DISPLAY_GERER_LOGICIELS
                       END-IF
                   END-IF
               END-IF.

           DISPLAY_GERER_ENTREPRISES.
               DISPLAY "| GESTION DES ENTREPRISES |"
               DISPLAY "<- 0 : Retour Menu Admin"
               DISPLAY " - 1 : Ajouter une entreprise"
               DISPLAY " - 2 : Lister les entreprises"
               ACCEPT WreponseAccept
               PERFORM WITH TEST BEFORE UNTIL WreponseAccept = 0
               OR WreponseAccept = 1 OR WreponseAccept = 2
                   DISPLAY "Réponse incorrecte, réessayez"
                   ACCEPT WreponseAccept
               END-PERFORM
               IF WreponseAccept = 0
                   DISPLAY " "
                   PERFORM DISPLAY_ADMIN_MENU
               ELSE
                   IF WreponseAccept = 1
                       DISPLAY " "
                       PERFORM AFFICHER_ENTREPRISES
                       DISPLAY " "
                       PERFORM AJOUTER_ENTREPRISE
                       DISPLAY " "
                       PERFORM DISPLAY_GERER_ENTREPRISES
                   ELSE
                       IF WreponseAccept = 2
                           DISPLAY " "
                           PERFORM AFFICHER_ENTREPRISES
                           DISPLAY " "
                           PERFORM DISPLAY_GERER_ENTREPRISES
                       END-IF
                   END-IF
              END-IF.



      ** FONCTIONS ENTREPRISE
           AFFICHER_ENTREPRISES.
               MOVE 0 TO Wfin
               DISPLAY "Liste des entreprises existantes :"
               OPEN INPUT fentr
               PERFORM WITH TEST AFTER UNTIL Wfin = 1
                  READ fentr
                  AT END
                     MOVE 1 TO Wfin
                  NOT AT END
                     DISPLAY " - " fentr_nom
                  END-READ
               END-PERFORM
               CLOSE fentr.

           AJOUTER_ENTREPRISE.
               DISPLAY "- AJOUTER_ENTREPRISE -"
               DISPLAY "Nom :"
                  ACCEPT Wfentr_nom
                  MOVE Function Upper-case(Wfentr_nom) TO Wfentr_nom
                  MOVE Wfentr_nom to param_EXISTE_ENTREPRISE_nom
                  PERFORM EXISTE_ENTREPRISE
               IF sortie_EXISTE_ENTREPRISE_existe = 0
                   PERFORM WITH TEST AFTER UNTIL
                   Function Upper-case(Wfentr_taille) = "G"
                   OR Function Upper-case(Wfentr_taille) = "M"
                   OR Function Upper-case(Wfentr_taille) = "P"
                       DISPLAY " "
                    DISPLAY "Taille "
                    "('G' : Grande, 'M': Moyenne, 'P': Petite) :"
                       ACCEPT Wfentr_taille
                   END-PERFORM
                   PERFORM WITH TEST AFTER UNTIL
                   Function Upper-case(Wfentr_origine) = "V"
                   OR Function Upper-case(Wfentr_origine) = "B"
                       DISPLAY " "
                       DISPLAY "Type "
                       "('V': Privé, 'B': Public) :"
                       ACCEPT Wfentr_origine
                  END-PERFORM
                   DISPLAY " "
                   DISPLAY "Statut juridique :"
                   ACCEPT Wfentr_statutJur
                   MOVE Function Upper-case(Wfentr_statutJur)
                   TO Wfentr_statutJur
                   PERFORM GET_NEW_ID_ENTREPRISE
                   MOVE Wfentr_nom to fentr_nom
                   MOVE Wfentr_taille to fentr_taille
                   MOVE Wfentr_origine to fentr_origine
                   MOVE Wfentr_statutJur to fentr_statutJur
                   MOVE sortie_GET_NEW_ID_ENTREPRISE_id TO fentr_id
                   OPEN EXTEND fentr
                       WRITE tamp_fentr
                       END-WRITE
                   CLOSE fentr
                   DISPLAY " "
                   DISPLAY "L'entreprise a bien été ajoutée"
               ELSE
                   DISPLAY " "
                   DISPLAY "/!\ Cette entreprise existe déja"
                   PERFORM DISPLAY_GERER_ENTREPRISES
               END-IF.

      * params : nom
      * sorties : id
           GET_ID_ENTREPRISE.
               MOVE 0 TO Wfin
               MOVE 0 TO Wtrouve
               OPEN INPUT fentr
               PERFORM WITH TEST AFTER UNTIL Wfin = 1 OR Wtrouve = 1
                   READ fentr
                   AT END
                       MOVE 1 TO Wfin
                   NOT AT END
                       IF Function Upper-case(fentr_nom) =
                       Function Upper-case(param_GET_ID_ENTREPRISE_nom)
                           MOVE fentr_id TO sortie_GET_ID_ENTREPRISE_id
                           MOVE 1 TO Wtrouve
                       END-IF
                   END-READ
               END-PERFORM
               CLOSE fentr.

      * sorties : id
           GET_NEW_ID_ENTREPRISE.
               MOVE 0 TO Wfin
               MOVE 0 TO fentr_id
               OPEN INPUT fentr
               PERFORM WITH TEST AFTER UNTIL Wfin = 1
                  READ fentr
                  AT END
                     MOVE 1 TO Wfin
                  NOT AT END
                     MOVE fentr_id TO sortie_GET_NEW_ID_ENTREPRISE_id
                     ADD 1 TO sortie_GET_NEW_ID_ENTREPRISE_id
                  END-READ
               END-PERFORM
               CLOSE fentr.

      * params : nom
      * sorties : existe
           EXISTE_ENTREPRISE.
               MOVE 0 to sortie_EXISTE_ENTREPRISE_existe
               MOVE 0 TO Wfin
               MOVE 0 TO Wtrouve
               OPEN INPUT fentr
               PERFORM WITH TEST AFTER UNTIL Wfin = 1 OR Wtrouve = 1
                  READ fentr
                  AT END
                     MOVE 1 TO Wfin
                  NOT AT END
                     IF Function Upper-case(fentr_nom) =
                     Function Upper-case(param_EXISTE_ENTREPRISE_nom)
                     THEN
                        MOVE 1 TO Wtrouve
                        MOVE 1 TO sortie_EXISTE_ENTREPRISE_existe
                     END-IF
                  END-READ
               END-PERFORM
               CLOSE fentr.
      ** FONCTIONS TICKETS
           AFFICHER_TOUS_TICKETS.
               DISPLAY "Liste de tous les tickets: "
                   OPEN INPUT ftick
                   MOVE 0 TO Wfin
                   PERFORM WITH TEST AFTER UNTIL Wfin = 1
                      READ ftick
                      AT END
                         MOVE 1 TO Wfin
                      NOT AT END
                           DISPLAY " "
                           DISPLAY "ID du ticket: " ftick_id
                           DISPLAY "Date de création: "
                           ftick_datecreation
                           DISPLAY "ID du logiciel: " ftick_idlogiciel
                           DISPLAY "ID du créateur " ftick_idcreateur
                           IF ftick_nomMainteneur = SPACE
                           OR ftick_nomMainteneur = LOW-VALUE
                               DISPLAY "Non pris en charge"
                           ELSE
                               DISPLAY "Nom du mainteneur: "
                               ftick_nomMainteneur
                               IF ftick_datefermeture = SPACE
                               OR ftick_datefermeture = LOW-VALUE
                                   DISPLAY "Non clos"
                               ELSE
                                   DISPLAY "Date de fermeture: "
                                   ftick_datefermeture
                               END-IF
                           END-IF
                           DISPLAY "Description: " ftick_description
                      END-READ
                   END-PERFORM
                   CLOSE ftick.

           AJOUTER_TICKET.
               DISPLAY "Création d'un ticket"
               DISPLAY " "
               PERFORM GET_NEW_ID_TICKET
               MOVE SPACE TO ftick_nomMainteneur
               MOVE sortie_GET_NEW_ID_TICKET_id TO ftick_id
               MOVE sortie_CREATE_EMPLOYE_IF_NOT_EXIST_id
               TO ftick_idcreateur
               DISPLAY "Nom du logiciel concerné :"
               ACCEPT param_EXISTE_LOGICIEL_nom
               MOVE Function Upper-case(param_EXISTE_LOGICIEL_nom)
               TO param_EXISTE_LOGICIEL_nom
               PERFORM EXISTE_LOGICIEL
               IF sortie_EXISTE_LOGICIEL_existe = 1
                   MOVE sortie_EXISTE_LOGICIEL_id to ftick_idlogiciel
                   DISPLAY "Description :"
                   ACCEPT ftick_description
                   MOVE FUNCTION CURRENT-DATE TO WS-CURRENT-DATE-DATA
                   MOVE WS-CURRENT-DATE TO ftick_datecreation
                   OPEN I-O ftick
                       WRITE tamp_ftick
                       END-WRITE
                   CLOSE ftick
                   MOVE ftick_idlogiciel
                   TO param_INCR_NB_TICKETS_LOGICIEL_idLog
                   PERFORM INCR_NB_TICKETS_LOGICIEL
                   DISPLAY " "
                   DISPLAY "Le ticket a bien été créé"
                   DISPLAY " "
               ELSE
                   DISPLAY " "
                   DISPLAY "/!\ Nom de logiciel inconnu"
                   DISPLAY " "
                   PERFORM DISPLAY_CLIENT_MENU.

           AFFICHER_TICKETS_DISPONIBLES.
               DISPLAY "Liste des tickets non pris en charge: "
               MOVE 0 TO Wfin
               OPEN INPUT ftick
               PERFORM WITH TEST AFTER UNTIL Wfin = 1
                   READ ftick
                   AT END
                       MOVE 1 TO Wfin
                   NOT AT END
                       IF NOT (ftick_nomMainteneur <> SPACE
                       AND ftick_nomMainteneur <> LOW-VALUE)
                           DISPLAY " "
                           DISPLAY "ID du ticket: " ftick_id
                           DISPLAY "Date de création: "
                           ftick_datecreation
                           DISPLAY "ID du logiciel: " ftick_idlogiciel
                           DISPLAY "ID du créateur " ftick_idcreateur
                           IF ftick_nomMainteneur = SPACE
                           OR ftick_nomMainteneur = LOW-VALUE
                               DISPLAY "Non pris en charge"
                           ELSE
                               DISPLAY "Nom du mainteneur: "
                               ftick_nomMainteneur
                           END-IF
                           DISPLAY "Description: " ftick_description
                       END-IF
                    END-READ
               END-PERFORM
               CLOSE ftick.

      * params : nomMainteneur
           AFFICHER_TICKETS_EN_CHARGE.
               DISPLAY
               "Liste des tickets que vous avez pris en charge: "
               OPEN INPUT ftick
               MOVE 0 TO Wfin
               PERFORM WITH TEST AFTER UNTIL Wfin = 1
                   READ ftick
                   AT END
                       MOVE 1 TO Wfin
                   NOT AT END
                       IF Function Upper-case(ftick_nomMainteneur) =
                       Function
           Upper-case(entree_AFFICHER_TICKETS_EN_CHARGE_nomMainteneur)
                           DISPLAY " "
                           DISPLAY "ID du ticket: " ftick_id
                           DISPLAY "Date de création: "
                           ftick_datecreation
                           DISPLAY "ID du logiciel: " ftick_idlogiciel
                           DISPLAY "ID du créateur " ftick_idcreateur
                           IF ftick_nomMainteneur = SPACE
                           OR ftick_nomMainteneur = LOW-VALUE
                               DISPLAY "Non pris en charge"
                           ELSE
                               DISPLAY "Nom du mainteneur: "
                               ftick_nomMainteneur
                           END-IF
                           DISPLAY "Description: " ftick_description
                       END-IF
                    END-READ
               END-PERFORM
               CLOSE ftick.

      * params : idCreateur
           AFFICHER_TICKETS_EMPLOYE.
               DISPLAY "Liste de vos tickets: "
               MOVE 0 TO Wfin
               MOVE 0 TO Wfdz
               MOVE 0 TO Wtrouve
               MOVE param_AFFICHER_TICKETS_EMPLOYE_idCreateur
               TO ftick_idcreateur
               OPEN INPUT ftick
               START ftick KEY IS = ftick_idcreateur
               NOT INVALID KEY
                   PERFORM WITH TEST AFTER UNTIL Wfin = 1 OR Wfdz = 1
                       READ ftick NEXT
                       AT END
                           MOVE 1 TO Wfin
                           MOVE 1 TO Wfdz
                       NOT AT END
                           IF ftick_idcreateur = 
                           param_AFFICHER_TICKETS_EMPLOYE_idCreateur
                               DISPLAY " "
                               DISPLAY "ID du ticket: " ftick_id
                               DISPLAY "Date de création: "
                               ftick_datecreation
                               DISPLAY "ID du logiciel: "
                               ftick_idlogiciel
                               DISPLAY "ID du créateur "
                               ftick_idcreateur
                               IF ftick_nomMainteneur = SPACE
                               OR ftick_nomMainteneur = LOW-VALUE
                                   DISPLAY "Non pris en charge"
                               ELSE
                                   DISPLAY "Nom du mainteneur: "
                                   ftick_nomMainteneur
                               END-IF
                               DISPLAY "Description: "
                               ftick_description
                           ELSE
                               MOVE 1 TO Wfdz
                           END-IF
                       END-READ
                   END-PERFORM
               END-START
               CLOSE ftick.

      * params : idCreateur
           AFFICHER_TICKETS_EMPLOYE_LOGICIEL.
               MOVE 0 TO Wfin
               MOVE 0 TO Wfdz
               MOVE 0 TO Wtrouve
               DISPLAY "Nom du logiciel :"
               ACCEPT param_EXISTE_LOGICIEL_nom
               MOVE Function Upper-case(param_EXISTE_LOGICIEL_nom)
               TO param_EXISTE_LOGICIEL_nom
               PERFORM EXISTE_LOGICIEL
               IF sortie_EXISTE_LOGICIEL_existe = 1
                   DISPLAY " "
                   DISPLAY "Liste de vos tickets pour le logiciel '"
                   param_EXISTE_LOGICIEL_nom "': "
                   MOVE param_AFFICHER_TICKETS_EMPLOYE_idCreateur
                   TO ftick_idcreateur
                   OPEN INPUT ftick
                   START ftick KEY IS = ftick_idcreateur
                   NOT INVALID KEY
                   PERFORM WITH TEST AFTER UNTIL Wfin = 1 OR Wfdz = 1
                       READ ftick NEXT
                       AT END
                           MOVE 1 TO Wfin
                           MOVE 1 TO Wfdz
                       NOT AT END
                           IF ftick_idcreateur = 
                           param_AFFICHER_TICKETS_EMPLOYE_idCreateur
                           AND
                           sortie_EXISTE_LOGICIEL_id = ftick_idLogiciel
                               DISPLAY " "
                               DISPLAY "ID du ticket: " ftick_id
                               DISPLAY "Date de création: "
                               ftick_datecreation
                               DISPLAY "ID du logiciel: "
                               ftick_idlogiciel
                               DISPLAY "ID du créateur "
                               ftick_idcreateur
                               IF ftick_nomMainteneur = SPACE
                               OR ftick_nomMainteneur = LOW-VALUE
                                   DISPLAY "Non pris en charge"
                               ELSE
                                   DISPLAY "Nom du mainteneur: "
                                   ftick_nomMainteneur
                               END-IF
                               DISPLAY "Description: "
                               ftick_description
                           ELSE
                               MOVE 1 TO Wfdz
                           END-IF
                       END-READ
                   END-PERFORM
                   END-START
                   CLOSE ftick
               ELSE
                   DISPLAY " "
                   DISPLAY "/!\ Ce logiciel n'existe pas"
                   DISPLAY " "
               END-IF.

      * sorties : id
           GET_NEW_ID_TICKET.
               MOVE 0 TO Wfin
               MOVE 0 TO ftick_id
               OPEN INPUT ftick
               PERFORM WITH TEST AFTER UNTIL Wfin = 1
                  READ ftick
                  AT END
                     MOVE 1 TO Wfin
                  NOT AT END
                     MOVE ftick_id TO sortie_GET_NEW_ID_TICKET_id
                     ADD 1 TO sortie_GET_NEW_ID_TICKET_id
                  END-READ
               END-PERFORM
               CLOSE ftick.

           CLORE_TICKET.
               DISPLAY "Veuillez entrer l'ID du ticket que vous"
               DISPLAY "souhaitez clore :"
               ACCEPT param_EXISTE_TICKET_id
               PERFORM EXISTE_TICKET
               IF sortie_EXISTE_TICKET_existe = 1
                   OPEN I-O ftick
                   READ ftick INTO tamp_ftick
                       KEY IS ftick_id
                       NOT INVALID KEY
                           IF entree_DISPLAY_ADMIN_MENU_nomMainteneur =
                           ftick_nomMainteneur OR ftick_datefermeture
                               MOVE FUNCTION CURRENT-DATE
                               TO WS-CURRENT-DATE-DATA
                               MOVE WS-CURRENT-DATE
                               TO ftick_datefermeture
                               REWRITE tamp_ftick
                           ELSE
                               DISPLAY " "
                               DISPLAY
                               "Vous ne pouvez pas clore ce ticket"
                               DISPLAY "Raisons possibles :"
                               DISPLAY
                               " - Ticket pris en charge par un autre"
                               DISPLAY " - Ticket non pris en charge"
                               DISPLAY " - Ticket déja clos"
                               DISPLAY " "
                               PERFORM DISPLAY_GERER_TICKETS
                           END-IF
                   END-READ
                   CLOSE ftick
                   DISPLAY " "
                   DISPLAY "Le ticket a bien été clos"
                   DISPLAY " "
               ELSE
                   DISPLAY " "
                   DISPLAY "/!\ ID de ticket inconnu"
                   DISPLAY " "
                   PERFORM DISPLAY_GERER_TICKETS
               END-IF.

           PRENDRE_EN_CHARGE_TICKET.
               DISPLAY "Veuillez entrer l'ID du ticket que vous"
               DISPLAY "souhaitez prendre en charge :"
               ACCEPT param_EXISTE_TICKET_id
               PERFORM EXISTE_TICKET
               IF sortie_EXISTE_TICKET_existe = 1
                   OPEN I-O ftick
                   READ ftick INTO tamp_ftick
                       KEY IS ftick_id
                       NOT INVALID KEY
                           IF (ftick_nomMainteneur = SPACE
                           OR ftick_nomMainteneur = LOW-VALUE)
                            MOVE entree_DISPLAY_ADMIN_MENU_nomMainteneur
                            TO ftick_nomMainteneur
                            REWRITE tamp_ftick
                           ELSE
                            DISPLAY "Ce ticket est déja pris en charge"
                            DISPLAY ftick_nomMainteneur
                            PERFORM DISPLAY_GERER_TICKETS
                           END-IF
                   END-READ
                   CLOSE ftick
                   DISPLAY " "
                   DISPLAY "Le ticket a bien été pris en charge"
                   DISPLAY " "
               ELSE
                   DISPLAY " "
                   DISPLAY "/!\ ID de ticket inconnu"
                   DISPLAY " "
                   PERFORM DISPLAY_GERER_TICKETS
               END-IF.

      * params : id
      * sorties : existe
           EXISTE_TICKET.
               MOVE 0 TO sortie_EXISTE_TICKET_existe
               MOVE param_EXISTE_TICKET_id TO tamp_ftick
               OPEN I-O ftick
                   READ ftick INTO tamp_ftick
                       KEY IS ftick_id
                       NOT INVALID KEY
                           MOVE 1 TO sortie_EXISTE_TICKET_existe
                   END-READ
               CLOSE ftick.

           SUPPRIMER_TICKET.
               DISPLAY "Veuillez entrer l'ID du ticket que vous"
               "souhaitez supprimer :"
               ACCEPT param_EXISTE_TICKET_id
               PERFORM EXISTE_TICKET
               IF sortie_EXISTE_TICKET_existe = 1
                   OPEN I-O ftick
                   READ ftick INTO tamp_ftick
                       KEY IS ftick_id
                       INVALID KEY
                           DISPLAY " "
                           DISPLAY "Lecture impossible"
                       NOT INVALID KEY
                           MOVE sortie_GET_ID_ENTREPRISE_id
                           TO param_EXISTE_EMPLOYE_idEntr
                           MOVE param_CREATE_EMPLOYE_IF_NOT_EXIST_nom
                           TO param_EXISTE_EMPLOYE_nom
                           MOVE param_CREATE_EMPLOYE_IF_NOT_EXIST_prenom
                           TO param_EXISTE_EMPLOYE_prenom
                           PERFORM EXISTE_EMPLOYE
                           IF sortie_EXISTE_EMPLOYE_idEmpl =
                           ftick_idCreateur
                           AND (ftick_datefermeture = SPACE
                           OR ftick_datefermeture = LOW-VALUE)
                               DELETE ftick
                               MOVE ftick_idlogiciel
                               TO param_SUB_NB_TICKETS_LOGICIEL_idLog
                               PERFORM SUB_NB_TICKETS_LOGICIEL
                               DISPLAY " "
                               DISPLAY "Le ticket a bien été supprime"
                           ELSE
                               DISPLAY " "
                               DISPLAY
                               "Vous ne pouvez pas supprimer ce ticket"
                               DISPLAY "Raisons possibles :"
                               DISPLAY
                               " - Vous n'etes pas le createur"
                               DISPLAY " - Ticket clos"
                           END-IF
                   END-READ
                   CLOSE ftick
               ELSE
                   DISPLAY " "
                   DISPLAY "/!\ ID de ticket inconnu"
               END-IF.

      ** FONCTIONS EMPLOYES
      

      * params : nom, prenom
      * sorties : id
           CREATE_EMPLOYE_IF_NOT_EXIST.
               MOVE param_EXISTE_ENTREPRISE_nom
               TO param_GET_ID_ENTREPRISE_nom
               PERFORM GET_ID_ENTREPRISE
               MOVE sortie_GET_ID_ENTREPRISE_id
               TO param_EXISTE_EMPLOYE_idEntr
               MOVE param_CREATE_EMPLOYE_IF_NOT_EXIST_nom
               TO param_EXISTE_EMPLOYE_nom
               MOVE param_CREATE_EMPLOYE_IF_NOT_EXIST_prenom
               TO param_EXISTE_EMPLOYE_prenom
               PERFORM EXISTE_EMPLOYE
               IF sortie_EXISTE_EMPLOYE_existe = 0
                   MOVE param_EXISTE_ENTREPRISE_nom
                   TO param_GET_ID_ENTREPRISE_nom
                   PERFORM GET_ID_ENTREPRISE
                   PERFORM GET_NEW_ID_EMPLOYE
                   MOVE sortie_GET_NEW_ID_EMPLOYE_id to fempl_id
                   MOVE sortie_GET_ID_ENTREPRISE_id
                   TO fempl_identreprise
                   MOVE param_CREATE_EMPLOYE_IF_NOT_EXIST_nom
                   TO fempl_nom
                   MOVE param_CREATE_EMPLOYE_IF_NOT_EXIST_prenom
                   TO fempl_prenom
                   DISPLAY "Vous etes un nouveau client"
                   DISPLAY "Veuillez entrer votre numero de telephone :"
                   ACCEPT fempl_numerotel
                   OPEN I-O fempl
                   WRITE tamp_fempl
                   END-WRITE
                   CLOSE fempl
                   MOVE sortie_GET_NEW_ID_EMPLOYE_id
                   TO sortie_CREATE_EMPLOYE_IF_NOT_EXIST_id
               ELSE
                   MOVE sortie_EXISTE_EMPLOYE_idEmpl
                   TO sortie_CREATE_EMPLOYE_IF_NOT_EXIST_id
               END-IF.

      * params : nom, prenom, idEntr
      * sorties : existe, idEmpl
           EXISTE_EMPLOYE.
               MOVE 0 TO Wfin
               MOVE 0 TO Wtrouve
               MOVE 0 TO sortie_EXISTE_EMPLOYE_existe
               MOVE param_EXISTE_EMPLOYE_nom TO fempl_nom
               OPEN INPUT fempl
               START fempl KEY IS = fempl_nom
               NOT INVALID KEY
                   PERFORM WITH TEST AFTER UNTIL Wfin = 1 OR Wtrouve = 1
                       READ fempl NEXT
                       AT END
                           MOVE 1 TO Wfin
                       NOT AT END
                           IF Function Upper-case(fempl_prenom) =
                           Function
                           Upper-case(param_EXISTE_EMPLOYE_prenom)
                           AND fempl_identreprise =
                           param_EXISTE_EMPLOYE_idEntr
                               MOVE 1 TO sortie_EXISTE_EMPLOYE_existe
                               MOVE 1 TO Wtrouve
                               MOVE fempl_id
                               TO sortie_EXISTE_EMPLOYE_idEmpl
                   END-IF
                       END-READ
                   END-PERFORM
               END-START
               CLOSE fempl.

           AFFICHER_EMPLOYES.
               DISPLAY "Tous les employés:"
               OPEN INPUT fempl
               MOVE 0 TO Wfin
               PERFORM WITH TEST AFTER UNTIL Wfin = 1
                  READ fempl NEXT
                  AT END
                      MOVE 1 TO Wfin
                  NOT AT END
                      DISPLAY " "
                      DISPLAY "Nom: " fempl_nom
                      DISPLAY "Prénom: " fempl_prenom
                      DISPLAY "Numéro de téléphone: " fempl_numeroTel
                      DISPLAY "ID de l'employé: " fempl_id
                      DISPLAY "ID de l'entreprise: " fempl_identreprise
                  END-READ
               END-PERFORM
               CLOSE fempl.

      * sorties : id
           GET_NEW_ID_EMPLOYE.
               MOVE 0 TO Wfin
               MOVE 0 TO fempl_id
               OPEN INPUT fempl
               PERFORM WITH TEST AFTER UNTIL Wfin = 1
                  READ fempl
                  AT END
                     MOVE 1 TO Wfin
                  NOT AT END
                     MOVE fempl_id TO sortie_GET_NEW_ID_EMPLOYE_id
                     ADD 1 TO sortie_GET_NEW_ID_EMPLOYE_id
                  END-READ
               END-PERFORM
               CLOSE fempl.

      ** FONCTIONS LOGICIELS

      * params : nom
      * sorties : existe, id
           EXISTE_LOGICIEL.
               MOVE 0 to sortie_EXISTE_LOGICIEL_existe
               MOVE param_EXISTE_LOGICIEL_nom TO flog_nom
               OPEN INPUT flog
                  READ flog INTO tamp_flog
                     KEY IS flog_nom
                     NOT INVALID KEY
                        MOVE 1 TO sortie_EXISTE_LOGICIEL_existe
                        MOVE flog_id TO sortie_EXISTE_LOGICIEL_id
                  END-READ

               CLOSE flog.

      * params : idLog
           INCR_NB_TICKETS_LOGICIEL.
               MOVE param_INCR_NB_TICKETS_LOGICIEL_idLog TO flog_id
               OPEN I-O flog
                  READ flog INTO tamp_flog
                     KEY IS flog_id
                     NOT INVALID KEY
                        ADD 1 TO flog_nbtickets
                        REWRITE tamp_flog
                  END-READ
               CLOSE flog.

      * params : idLog
           SUB_NB_TICKETS_LOGICIEL.
               MOVE param_SUB_NB_TICKETS_LOGICIEL_idLog TO flog_id
               OPEN I-O flog
                  READ flog INTO tamp_flog
                     KEY IS flog_id
                     NOT INVALID KEY
                        SUBTRACT 1 FROM flog_nbtickets
                        REWRITE tamp_flog
                  END-READ
               CLOSE flog.
           
      * params : idLog     
           AFFICHER_NB_TICKETS_LOGICIEL.
               DISPLAY "- AFFICHER NOMBRE DE TICKETS POUR UN LOGICIEL -"
               DISPLAY " "
               DISPLAY "Nom logiciel :"
               ACCEPT param_EXISTE_LOGICIEL_nom
               MOVE Function Upper-case(param_EXISTE_LOGICIEL_nom)
               TO param_EXISTE_LOGICIEL_nom
               PERFORM EXISTE_LOGICIEL
               IF sortie_EXISTE_LOGICIEL_existe = 1
                   MOVE param_AFFICHER_NB_TICKETS_LOGICIEL_idLog
                   TO flog_id
                   OPEN INPUT flog
                   READ flog INTO tamp_flog
                       KEY IS flog_id
                   NOT INVALID KEY
                       DISPLAY "Nombre de tickets pour ce logiciel :"
                       DISPLAY flog_nbtickets
                   END-READ
                   CLOSE flog
               ELSE
                   DISPLAY " "
                   DISPLAY "/!\ Ce logiciel n'existe pas"
                   DISPLAY " "
               END-IF.

           AJOUTER_LOGICIEL.
               DISPLAY "Ajout d'un nouveau logiciel :"
               DISPLAY " "
               DISPLAY "Nom logiciel :"
               ACCEPT param_EXISTE_LOGICIEL_nom
               MOVE Function Upper-case(param_EXISTE_LOGICIEL_nom)
               TO param_EXISTE_LOGICIEL_nom
               PERFORM EXISTE_LOGICIEL
               IF sortie_EXISTE_LOGICIEL_existe = 0
                   DISPLAY " "
                   DISPLAY "Type de logiciel :"
                   DISPLAY " - 1 : ERP"
                   DISPLAY " - 2 : CRM"
                   DISPLAY " - 3 : HRP"
                   DISPLAY " - 4 : Libre"
                   DISPLAY " - 5 : Autre"
                   PERFORM WITH TEST AFTER UNTIL
                   Wflog_typelogiciel = "1"
                   OR Wflog_typelogiciel = "2"
                   OR Wflog_typelogiciel = "3"
                   OR Wflog_typelogiciel = "4"
                   OR Wflog_typelogiciel = "5"
                       DISPLAY "Entrez une valeur :"
                       ACCEPT Wflog_typelogiciel
                   END-PERFORM
                   MOVE Wflog_typelogiciel TO flog_typelogiciel
                   PERFORM GET_NEW_ID_LOGICIEL
                   MOVE sortie_GET_NEW_ID_LOGICIEL_id TO flog_id
                   MOVE 0 to flog_nbTickets
                   MOVE param_EXISTE_LOGICIEL_nom TO flog_nom
                   OPEN I-O flog
                       WRITE tamp_flog
                       END-WRITE
                   CLOSE flog
                   DISPLAY " "
                   DISPLAY "Le logiciel a bien été ajouté"
               ELSE
                   DISPLAY " "
                   DISPLAY "/!\ Ce logiciel existe déja"
               END-IF.

           GET_NEW_ID_LOGICIEL.
               MOVE 0 TO Wfin
               MOVE 0 TO flog_id
               OPEN INPUT flog
               PERFORM WITH TEST AFTER UNTIL Wfin = 1
                  READ flog
                  AT END
                     MOVE 1 TO Wfin
                  NOT AT END
                     MOVE flog_id TO sortie_GET_NEW_ID_LOGICIEL_id
                     ADD 1 TO sortie_GET_NEW_ID_LOGICIEL_id
                  END-READ
               END-PERFORM
               CLOSE flog.

           AFFICHER_LOGICIELS.
               MOVE 0 TO Wfin
               DISPLAY "Liste des logiciels existants :"
               OPEN INPUT flog
               PERFORM WITH TEST AFTER UNTIL Wfin = 1
                  READ flog
                  AT END
                     MOVE 1 TO Wfin
                  NOT AT END
                     DISPLAY " - " flog_nom
                  END-READ
               END-PERFORM
               CLOSE flog.
