# Gestion de tickets Cobol

### __Projet cinématique des fichiers L3 MIAGE__

## <u>Readme utilisateur</u>

### Structure de l'application

```
Menu principal Gesticks
└───Quitter le logiciel
└───Connexion administrateur
│   └───Menu administrateur
│       └───Retour au menu principal
│       └───Gérer tickets
│       │   └───Retour au menu administrateur
│       │   └───Lister tickets disponibles
│       │   └───Prendre en charge un ticket
│       │   └───Clore un ticket
│       └───Gérer les logiciels
│       │   └───Retour au menu administrateur
│       │   └───Ajouter un logiciel
│       │   └───Lister les logiciels
│       └───Gérer les entreprises
│       │   └───Retour au menu administrateur
│       │   └───Ajouter une entreprise
│       │   └───Lister les entreprise
│       └───Statistiques
│           └───Retour au menu administrateur
│           └───Nombre de tickets pour un logiciel donné
│           └───Lister tous les employés utilisateurs
│           └───Lister tous les tickets créés
└───Connexion client
    └───Menu client
        └───Retour au menu principal
        └───Menu lister tickets
        │   └───Lister tous ses tickets
        │   └───Lister ses tickets sur un logiciel donné
        └───Créer un ticket
        └───Supprimer un ticket
```

### Manuel administrateur

Voici quelques informations sur le rôle administrateur :
* Le mot de passe utilisateur est "<u>password</u>".
* Il peut prendre en charge en ticket et le clore par la suite.
* Il peut ajouter des logiciels à la suite de logiciels proposée aux clients.
* Il peut ajouter des entreprises clientes.

### Manuel client

Voici quelques informations sur le rôle client :
* L'entreprise du client doit être enregistrée dans Gesticks par un administrateur, sinon, celui-ci ne pourra pas se connecter.
* Il peut créer des tickets
* Il peut supprimer un ticket
* Il peut lister tous ses tickets

## <u>Readme technique</u>


### Script Compiler + Executer

```
./compileAndLaunch.sh
```

### Commande pour compiler le programme cobol

```
cobc -x gesticks.cob
```

### Commande pour executer le programme cobol compilé

```
./gesticks
```

### Normalisation des variables E/S des procédures

```
param_FONCTION_NOM_nomVariable1
sortie_FONCTION_NOM_nomVariable2

<=>

f(nomVariable1){  
    return nomVariable2  
}
```

# A l'attention des testeurs /!\

## Important

* Si vous ne souhaitez pas utiliser le jeu de données ci-dessous, vous pouvez passer par le menu administrateur pour créer une entreprise ou un logiciel avant de vous connecter en tant que client !

## Jeu de données existant

Entreprises :
* Sigma
* Apside
* 4CAD

Logiciels :
* Word
* Excel
* Powerpoint
* Outlook